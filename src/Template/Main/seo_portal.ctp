<?php
    use Cake\Routing\Router;
?>
<div class="col-md-6 col-sm-6">
    <form method="post" accept-charset="utf-8" action="<?php echo Router::url('/', true); ?>links/search">
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <div class="form-group">
                    <input type="text" name="search" placeholder="Search Link(s) by title or description..." class="form-control">
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <?php 
                    echo $this->form->button('Search', ['class' => 'btn btn-md btn-primary']);
                ?>
            </div>
        </div>
    </form>
</div>