<?php

use Cake\Routing\Router;
use Cake\Core\Configure;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SEO Link Directory Portal</title>
    <?php echo $this->Html->css(array('bootstrap.min', 'all', 'dashboard'));?>
</head>
<body>
<input type="hidden" id="base_url" value="<?php echo $base_path = Router::url('/', true); ?>">
<input name="_token" id="_token" type="hidden" value="<?= $this->request->getParam('_csrfToken') ?>">

<main class="page-content">
    <?php if(isset($user) && !empty($user)): ?>
    <div class="sidebar">
        <?= $this->Html->link('<i class="fas fa-tachometer-alt"></i> Dashboard',['controller' => 'Users', 'action' => 'dashboard'],['escape' => false]); ?>
        <?= $this->Html->link('<i class="fas fa-user"></i> Users',['controller' => 'Users', 'action' => 'users'],['escape' => false]); ?>
        <?= $this->Html->link('<i class="fas fa-cat"></i> Categories',['controller' => 'Users', 'action' => 'categories'],['escape' => false]); ?>
        <?= $this->Html->link('<i class="fas fa-child"></i> Sub Categories',['controller' => 'Users', 'action' => 'subCategories'],['escape' => false]); ?>
        <?= $this->Html->link('<i class="fas fa-link"></i> Links',['controller' => 'Users', 'action' => 'links'],['escape' => false]); ?>
        <?= $this->Html->link('<i class="fas fa-sign-out-alt"></i> Logout',['controller' => 'Users', 'action' => 'logout'],['class' => 'logout', 'escape' => false]); ?>
    </div>
    <?php endif; ?>

    <div class=<?= isset($user) && !empty($user) ? "content" : "container"?>>
        <?= $this->fetch('content') ?>
    </div>
</div>

<?php echo $this->Html->script(array('jquery.min', 'bootstrap.min', 'custom')); ?>
</body>
</html>