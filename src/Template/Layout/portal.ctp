<?php

use Cake\Routing\Router;
use Cake\Core\Configure;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FCCU Portal</title>
    
    <?php echo $this->Html->css(array('bootstrap.min', 'style'));?>
</head>
<body>
<input type="hidden" id="base_url" value="<?php echo $base_path = Router::url('/', true); ?>">
<input name="_token" id="_token" type="hidden" value="<?= $this->request->getParam('_csrfToken') ?>">

<main class="page-content content-wrap">
    <!--Navbar -->
    <nav class="mb-4 navbar navbar-expand-lg main-menu">
        <h4 class="site-title">SEO Link Directory Portal</h4>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-4">
            <ul class="navbar-nav">
                <li class="nav-item dropdown settings">
                    <a class="nav-link settings-drpdn">Admin</a>
                </li>
                <li class="nav-item dropdown settings">
                    <a class="nav-link settings-drpdn">HR</a>
                </li>
                <li class="nav-item dropdown settings">
                    <a class="nav-link settings-drpdn">Manager</a>
                </li>
                <li class="nav-item dropdown settings">
                    <a class="nav-link settings-drpdn">New User</a>
                </li>
                <li class="nav-item dropdown settings">
                    <a class="nav-link settings-drpdn">Student</a>
                </li>
                <li class="nav-item dropdown settings">
                    <a class="nav-link settings-drpdn">Teacher</a>
                </li>
                <li class="nav-item dropdown settings">
                    <a class="nav-link settings-drpdn">Course</a>
                </li>
                <li class="nav-item dropdown settings">
                    <a class="nav-link settings-drpdn">Staff</a>
                </li>
            </ul>
        </div>
    </nav>
    <!--/.Navbar -->
    
    <!-- Breadcrumb -->
    <?php if(isset($heading)): ?>
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h1 class="heading"><?= $heading; ?></h1>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <!--/ Breadcrumb -->

    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
</div>

<?php echo $this->Html->script(array('jquery.min', 'bootstrap.min', 'custom')); ?>
</body>
</html>