<div class="row">
    <div class="col-md-6 offset-md-3">
        <?= $this->Flash->render(); ?>
        <div class="card mt-5">
            <div class="card-body">
                <?php echo $this->form->create(); ?>
                <div class="form-group">
                    <?php echo $this->form->input('user_email', ['type' => 'email', 'label' => 'User Email: <span class="required">*</span>', 'class' => 'form-control', 'required', 'escape' => false]); ?>
                </div>
                <div class="form-group clearfix">
                    <?php echo $this->form->input('user_password', ['type' => 'password', 'label' => 'User Password: <span class="required">*</span>', 'class' => 'form-control', 'required', 'escape' => false]); ?>
                </div>
                <div class="row justify-content-center">
                <?php 
                    echo $this->form->button('Login', ['class' => 'btn buttons']);
                    echo $this->form->end();
                ?>
                </div>
            </div>
        </div>
    </div>
</div>