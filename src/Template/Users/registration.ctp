<div class="row mb-5">
    <div class="col-md-6 offset-md-3">
        <?= $this->Flash->render(); ?>
        <div class="card mt-5">
            <div class="card-body">
                <?php echo $this->form->create(); ?>
                <div class="form-group">
                    <?php echo $this->form->input('user_name', ['label' => 'User Name: ', 'class' => 'form-control', 'disabled', 'value' => isset($user_data->user_name) ? $user_data->user_name : ""]); ?>
                </div>
                <div class="form-group">
                    <?php echo $this->form->input('user_email', ['type' => 'email', 'label' => 'User Email: ', 'class' => 'form-control', 'disabled', 'value' => isset($user_data->user_email) ? $user_data->user_email : ""]); ?>
                </div>
                <div class="form-group">
                    <div class="input number">
                        <label for="user_cnic">User CNIC: </label>
                        <input type="text"  name="user_cnic" class= 'form-control' data-inputmask="'mask': '99999-9999999-9'"  placeholder="XXXXX-XXXXXXX-X" value="<?= isset($user_data->user_cnic) ? $user_data->user_cnic : ""; ?>" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input date">
                        <label for="user_dob">Date of Birth: <span class="required">*</span></label>
                        <input type="date" id="user-dob" name="user_dob" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <?php 
                        echo $this->Form->input(
                            'user_gender',
                            [
                                'type' => 'select',
                                'options' => ['M' => 'Male', 'F' => 'Female', 'T' => 'Transgender'],
                                'class' => 'form-control',
                                'label' => 'User Gender: <span class="required">*</span>',
                                'escape' => false,
                                'required',
                            ]
                        );
                    ?>
                </div>
                <div class="form-group">
                    <div class="input number">
                        <label for="user_phone">User Phone: <span class="required">*</span></label>
                        <input type="text"  name="user_phone" class='form-control' data-inputmask="'mask': '0399-9999999'" placeholder="XXXX-XXXXXXX" required="required">
                    </div>
                </div>
                <div class="form-group">
                    <label><b>Please Upload the image of your CNIC (jpg or jpeg only)</b></label>
                </div>
                <div class="form-group">
                    <?php echo $this->form->input('user_cnic_front', ['type' => 'file', 'label' => 'User CNIC Front Image (jpg or jpeg only): <span class="required">*</span>', 'class' => 'form-control', 'accept' => 'image/jpeg', 'required', 'escape' => false]); ?>                
                </div>
                <div class="form-group">
                    <?php echo $this->form->input('user_cnic_back', ['type' => 'file', 'label' => 'User CNIC Back Image (jpg or jpeg only): <span class="required">*</span>', 'class' => 'form-control', 'accept' => 'image/jpeg', 'required', 'escape' => false]); ?>                
                </div>
                <div class="form-group">
                    <?php echo $this->form->input('user_pass', ['type' => 'password', 'label' => 'User Password (Minimun 8 alphanumeric characters): <span class="required">*</span>', 'class' => 'form-control', 'required', 'escape' => false]); ?>                
                </div>
                <div class="form-group">
                    <?php echo $this->form->input('user_confirm_pass', ['type' => 'password', 'label' => 'User Confirm Password (Minimun 8 alphanumeric characters): <span class="required">*</span>', 'class' => 'form-control', 'required', 'escape' => false]); ?>                
                </div>
                <div class="row justify-content-center">
                <?php 
                    echo $this->form->button('Register', ['class' => 'btn buttons']);
                    echo $this->form->end();
                ?>
                </div>
            </div>
        </div>
    </div>
</div>