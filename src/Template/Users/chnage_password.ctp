<div class="row">
    <div class="col-md-6 offset-md-3">
        <?= $this->Flash->render(); ?>
        <div class="card mt-5">
            <div class="card-body">
                <?php echo $this->form->create(); ?>
                <div class="form-group">
                    <?php echo $this->form->input('user_password:', ['type' => 'password', 'label' => 'Old Password: <span class="required">*</span>', 'class' => 'form-control', 'required', 'escape' => false]); ?>
                </div>
                <div class="form-group">
                    <?php echo $this->form->input('new_password', ['type' => 'password', 'label' => 'New Password: <span class="required">*</span>', 'class' => 'form-control', 'required', 'escape' => false]); ?>
                </div>
                <div class="form-group">
                    <?php echo $this->form->input('confirm_new_password', ['type' => 'password', 'label' => 'Confirm Password: <span class="required">*</span>', 'class' => 'form-control', 'required', 'escape' => false]); ?>
                </div>
                <div class="row justify-content-center">
                <?php 
                    echo $this->form->button('Change Password', ['class' => 'btn buttons']);
                    echo $this->form->end();
                ?>
                </div>
            </div>
        </div>
    </div>
</div>