<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Utility\Security;
use Cake\Validation\Validation;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Log\Log;

class MainController extends AppController
{
    public function seoPortal()
    {
        $this->viewBuilder()->setLayout('portal');
    }
}
