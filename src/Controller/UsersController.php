<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Utility\Security;
use Cake\Validation\Validation;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Log\Log;

class UsersController extends AppController
{
    public function login()
    {
        if($this->Auth->user()){
            $this->redirect(
                array(
                    "controller" => "Users", 
                    "action" => "dashboard",
                ),
            );
        }

        if($this->request->is('post')){
            $userTable = TableRegistry::getTableLocator()->get('Users');
            $email = $this->request->data['user_email'];
            $pass = $this->request->data['user_password'];
    
            $user = $userTable->find()->where(['email' => $email])->first();
            $user_password = $user->password;
            $password = md5($pass);
            $user_role = $user->user_role;

            if($user)
            {
                if($password == $user_password && $user_role == '1')
                {
                    $this->Auth->setUser($user);
                    $this->request->getSession()->write('profileData', $user);
                    return $this->redirect($this->Auth->redirectUrl());
                }else{
                    $this->Flash->set('Invalid Email or Password or You are not allowed to access this page.', ['element' => 'error']);
                }
            }
        }

        $this->viewBuilder()->setLayout('main');
    }

    public function logout()
    {
        $this->request->getSession()->delete('profileData');
        $this->redirect($this->Auth->logout());
    }

    public function dashboard()
    {
        $user = $this->request->getSession()->read('profileData');
        Log::write("debug", "dashboard => ".print_r($user, true));
        if(!isset($user->user_role)){
            $this->redirect(
                array(
                    "controller" => "Users", 
                    "action" => "login",
                ),
            );
        }
        $this->set("user", $user); 
        $this->viewBuilder()->setLayout('main');
    }
}
