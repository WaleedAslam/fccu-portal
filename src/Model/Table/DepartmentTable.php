<?php 

namespace App\Model\Table;

use Cake\ORM\Table;

class DepartmentTable extends Table
{
    public function initialize(array $config) {
        $this->table('department'); //define table name
        $this->primaryKey('department_id'); // primary key of users table
    }
}

?>