<?php 

namespace App\Model\Table;

use Cake\ORM\Table;

class JobTitleTable extends Table
{
    public function initialize(array $config) {
        $this->table('job_title'); //define table name
        $this->primaryKey('job_title_id'); // primary key of users table
    }
}

?>