<?php 

namespace App\Model\Table;

use Cake\ORM\Table;

class UsersTable extends Table
{
    public function initialize(array $config) {
        $this->table('users'); //define table name
        $this->primaryKey('user_id'); // primary key of users table
    }
}

?>